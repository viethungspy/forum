-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 26, 2021 lúc 03:17 AM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 7.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `forum`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'voyager::seeders.data_rows.roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'password', 'text', 'Password', 1, 0, 0, 0, 0, 0, '{}', 5),
(26, 4, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 6),
(27, 4, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 7),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(30, 4, 'role', 'select_dropdown', 'Admin', 1, 1, 1, 1, 1, 1, '{\"default\":\"user\",\"options\":{\"user\":\"User\",\"admin\":\"Admin\"}}', 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2021-11-23 21:41:32', '2021-11-23 21:41:32'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-11-23 21:41:32', '2021-11-23 21:41:32'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-11-23 21:41:32', '2021-11-23 21:41:32'),
(4, 'members', 'members', 'Member', 'Members', NULL, 'App\\Models\\Member', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-11-23 21:51:08', '2021-11-24 01:31:52');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `forum_categories`
--

CREATE TABLE `forum_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `accepts_threads` tinyint(1) NOT NULL DEFAULT 0,
  `newest_thread_id` int(10) UNSIGNED DEFAULT NULL,
  `latest_active_thread_id` int(10) UNSIGNED DEFAULT NULL,
  `thread_count` int(11) NOT NULL DEFAULT 0,
  `post_count` int(11) NOT NULL DEFAULT 0,
  `is_private` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `forum_categories`
--

INSERT INTO `forum_categories` (`id`, `title`, `description`, `accepts_threads`, `newest_thread_id`, `latest_active_thread_id`, `thread_count`, `post_count`, `is_private`, `created_at`, `updated_at`, `_lft`, `_rgt`, `parent_id`, `color`) VALUES
(1, 'hello everyone', 'say hello to me', 1, 2, 1, 2, 12, 0, '2021-11-23 22:02:10', '2021-11-24 01:22:18', 1, 2, NULL, '#007bff'),
(2, 'fdf', 'fdf', 1, NULL, NULL, 0, 0, 0, '2021-11-24 01:52:19', '2021-11-24 01:52:19', 3, 4, NULL, '#007bff');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `forum_posts`
--

CREATE TABLE `forum_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `thread_id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL,
  `sequence` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `forum_posts`
--

INSERT INTO `forum_posts` (`id`, `thread_id`, `author_id`, `content`, `post_id`, `sequence`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 1, 1, '???', NULL, 1, '2021-11-24 01:50:18', '2021-11-24 01:50:18', NULL),
(9, 1, 2, 'fsdafsd', NULL, 2, '2021-11-24 01:50:37', '2021-11-24 01:50:37', NULL),
(10, 2, 1, 'fasdf', NULL, 1, '2021-11-24 02:07:45', '2021-11-24 02:07:45', NULL),
(11, 1, 1, 'khong hieu', NULL, 3, '2021-11-24 02:53:22', '2021-11-24 02:53:22', NULL),
(12, 1, 2, 'khong hieu', NULL, 4, '2021-11-24 02:53:34', '2021-11-24 02:53:34', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `forum_threads`
--

CREATE TABLE `forum_threads` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinned` tinyint(1) DEFAULT 0,
  `locked` tinyint(1) DEFAULT 0,
  `first_post_id` int(10) UNSIGNED DEFAULT NULL,
  `last_post_id` int(10) UNSIGNED DEFAULT NULL,
  `reply_count` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `forum_threads`
--

INSERT INTO `forum_threads` (`id`, `category_id`, `author_id`, `title`, `pinned`, `locked`, `first_post_id`, `last_post_id`, `reply_count`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 2, 'đồng ý', 0, 0, 1, 12, 10, '2021-11-23 23:57:46', '2021-11-24 02:53:34', NULL),
(2, 1, 1, 'title 2', 0, 0, 10, 10, 0, '2021-11-24 02:07:45', '2021-11-24 02:07:45', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `forum_threads_read`
--

CREATE TABLE `forum_threads_read` (
  `thread_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `forum_threads_read`
--

INSERT INTO `forum_threads_read` (`thread_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 2, '2021-11-23 23:57:46', '2021-11-24 03:09:41'),
(1, 2, '2021-11-23 23:58:24', '2021-11-24 03:09:41'),
(1, 2, '2021-11-23 23:58:33', '2021-11-24 03:09:41'),
(1, 1, '2021-11-23 23:59:18', '2021-11-24 02:53:48'),
(1, 2, '2021-11-23 23:59:43', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:34:01', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:38:44', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:46:44', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:47:18', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:47:31', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:47:48', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:49:19', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:49:47', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:50:11', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:50:37', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 01:52:01', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:07:25', '2021-11-24 03:09:41'),
(2, 1, '2021-11-24 02:07:45', '2021-11-24 02:07:45'),
(2, 2, '2021-11-24 02:07:53', '2021-11-24 02:07:53'),
(2, 2, '2021-11-24 02:09:15', '2021-11-24 02:09:15'),
(1, 2, '2021-11-24 02:09:19', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:15:30', '2021-11-24 03:09:41'),
(2, 2, '2021-11-24 02:15:35', '2021-11-24 02:15:35'),
(1, 2, '2021-11-24 02:15:57', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:21:05', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:21:55', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:24:01', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:24:19', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:25:03', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:25:34', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:25:44', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:26:37', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:33:07', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:33:34', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:33:53', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:34:03', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:34:33', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:35:02', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:35:40', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:36:58', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:37:33', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:38:55', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:39:12', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:39:37', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:40:27', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:41:55', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:42:28', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:43:03', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:43:36', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:46:34', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:48:08', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:48:46', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:49:05', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:49:30', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:50:11', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:50:22', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:51:01', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:51:12', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:51:49', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:52:12', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:52:16', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:53:34', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:54:38', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:54:50', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:55:05', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:55:30', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:56:35', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 02:57:15', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:00:57', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:01:46', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:02:11', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:02:27', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:02:50', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:03:53', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:03:58', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:04:26', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:04:30', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:04:43', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:05:13', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:05:23', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:06:03', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:06:09', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:06:35', '2021-11-24 03:09:41'),
(1, 2, '2021-11-24 03:07:40', '2021-11-24 03:09:41');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `members`
--

INSERT INTO `members` (`id`, `name`, `email`, `password`, `remember_token`, `email_verified_at`, `created_at`, `updated_at`, `role`) VALUES
(1, 'Việt Hùng', 'viet.hung.2898@gmail.com', '$2y$10$CItAYUyQrhpYY7N091YB0uPTawZ/RWEx9/UviSeMpdX41W/rOdyeK', NULL, NULL, '2021-11-23 22:01:00', '2021-11-24 01:17:32', 'user'),
(2, 'user', 'admin@admin.com', '$2y$10$pEYdQuY8BI/rTwR99cjsV.dgHEjYwCrm5cI0Tvl0IoP/crRh46Tsq', NULL, NULL, '2021-11-23 23:56:00', '2021-11-24 01:31:28', 'admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-11-23 21:41:33', '2021-11-23 21:41:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-11-23 21:41:33', '2021-11-23 21:41:33', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2021-11-23 21:41:33', '2021-11-23 21:41:33', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2021-11-23 21:41:33', '2021-11-23 21:41:33', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2021-11-23 21:41:33', '2021-11-23 21:41:33', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2021-11-23 21:41:33', '2021-11-23 21:41:33', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2021-11-23 21:41:33', '2021-11-23 21:41:33', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2021-11-23 21:41:33', '2021-11-23 21:41:33', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2021-11-23 21:41:33', '2021-11-23 21:41:33', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2021-11-23 21:41:33', '2021-11-23 21:41:33', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2021-11-23 21:41:33', '2021-11-23 21:41:33', 'voyager.settings.index', NULL),
(11, 1, 'Members', '', '_self', NULL, NULL, NULL, 15, '2021-11-23 21:51:08', '2021-11-23 21:51:08', 'voyager.members.index', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_05_19_151759_create_forum_table_categories', 1),
(2, '2014_05_19_152425_create_forum_table_threads', 1),
(3, '2014_05_19_152611_create_forum_table_posts', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2015_04_14_180344_create_forum_table_threads_read', 1),
(7, '2015_07_22_181406_update_forum_table_categories', 1),
(8, '2015_07_22_181409_update_forum_table_threads', 1),
(9, '2015_07_22_181417_update_forum_table_posts', 1),
(10, '2016_05_24_114302_add_defaults_to_forum_table_threads_columns', 1),
(11, '2016_07_09_111441_add_counts_to_categories_table', 1),
(12, '2016_07_09_122706_add_counts_to_threads_table', 1),
(13, '2016_07_10_134700_add_sequence_to_posts_table', 1),
(14, '2018_11_04_211718_update_categories_table', 1),
(15, '2019_08_19_000000_create_failed_jobs_table', 1),
(16, '2019_09_07_210904_update_forum_category_booleans', 1),
(17, '2019_09_07_230148_add_color_to_categories', 1),
(18, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(19, '2020_03_22_050710_add_thread_ids_to_categories', 1),
(20, '2020_03_22_055827_add_post_id_to_threads', 1),
(21, '2020_12_02_233754_add_first_post_id_to_threads', 1),
(22, '2021_07_31_094750_add_fk_indices', 1),
(23, '2016_01_01_000000_add_voyager_user_fields', 2),
(24, '2016_01_01_000000_create_data_types_table', 2),
(25, '2016_05_19_173453_create_menu_table', 2),
(26, '2016_10_21_190000_create_roles_table', 2),
(27, '2016_10_21_190000_create_settings_table', 2),
(28, '2016_11_30_135954_create_permission_table', 2),
(29, '2016_11_30_141208_create_permission_role_table', 2),
(30, '2016_12_26_201236_data_types__add__server_side', 2),
(31, '2017_01_13_000000_add_route_to_menu_items_table', 2),
(32, '2017_01_14_005015_create_translations_table', 2),
(33, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 2),
(34, '2017_03_06_000000_add_controller_to_data_types_table', 2),
(35, '2017_04_21_000000_add_order_to_data_rows_table', 2),
(36, '2017_07_05_210000_add_policyname_to_data_types_table', 2),
(37, '2017_08_05_000000_add_group_to_settings_table', 2),
(38, '2017_11_26_013050_add_user_role_relationship', 2),
(39, '2017_11_26_015000_create_user_roles_table', 2),
(40, '2018_03_11_000000_add_user_settings', 2),
(41, '2018_03_14_000000_add_details_to_data_types_table', 2),
(42, '2018_03_16_000000_make_settings_value_nullable', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(2, 'browse_bread', NULL, '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(3, 'browse_database', NULL, '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(4, 'browse_media', NULL, '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(5, 'browse_compass', NULL, '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(6, 'browse_menus', 'menus', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(7, 'read_menus', 'menus', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(8, 'edit_menus', 'menus', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(9, 'add_menus', 'menus', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(10, 'delete_menus', 'menus', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(11, 'browse_roles', 'roles', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(12, 'read_roles', 'roles', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(13, 'edit_roles', 'roles', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(14, 'add_roles', 'roles', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(15, 'delete_roles', 'roles', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(16, 'browse_users', 'users', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(17, 'read_users', 'users', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(18, 'edit_users', 'users', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(19, 'add_users', 'users', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(20, 'delete_users', 'users', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(21, 'browse_settings', 'settings', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(22, 'read_settings', 'settings', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(23, 'edit_settings', 'settings', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(24, 'add_settings', 'settings', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(25, 'delete_settings', 'settings', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(26, 'browse_members', 'members', '2021-11-23 21:51:08', '2021-11-23 21:51:08'),
(27, 'read_members', 'members', '2021-11-23 21:51:08', '2021-11-23 21:51:08'),
(28, 'edit_members', 'members', '2021-11-23 21:51:08', '2021-11-23 21:51:08'),
(29, 'add_members', 'members', '2021-11-23 21:51:08', '2021-11-23 21:51:08'),
(30, 'delete_members', 'members', '2021-11-23 21:51:08', '2021-11-23 21:51:08');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2021-11-23 21:41:33', '2021-11-23 21:41:33'),
(2, 'user', 'Normal User', '2021-11-23 21:41:33', '2021-11-23 21:41:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$0z2f5MyBjNOOQcZ5bxdvLOqkzDd7ct8Dlw1sFOxH5oG9UMD4tCHIW', 'o2aFKiwJCMMcqv064C1stkBKD84bigC7Pxeld1ydksjrIGfv6EI7NoEo4pwW', NULL, '2021-11-23 21:41:51', '2021-11-23 21:41:51');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Chỉ mục cho bảng `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Chỉ mục cho bảng `forum_categories`
--
ALTER TABLE `forum_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_categories__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Chỉ mục cho bảng `forum_posts`
--
ALTER TABLE `forum_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_posts_thread_id_index` (`thread_id`);

--
-- Chỉ mục cho bảng `forum_threads`
--
ALTER TABLE `forum_threads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `forum_threads_category_id_index` (`category_id`);

--
-- Chỉ mục cho bảng `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `members_email_unique` (`email`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Chỉ mục cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Chỉ mục cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Chỉ mục cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Chỉ mục cho bảng `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `forum_categories`
--
ALTER TABLE `forum_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `forum_posts`
--
ALTER TABLE `forum_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `forum_threads`
--
ALTER TABLE `forum_threads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Các ràng buộc cho bảng `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
